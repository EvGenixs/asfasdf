package com.example.a3ways;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public void onClick(View view){
        switch (view.getId()){/*
            case R.id.signin:
                Intent sign = new Intent(MainActivity.this, signin_page.class);
                startActivity(sign);
                break;
                */
            case R.id.reg:
                Intent reg = new Intent(MainActivity.this,reg_page.class);
                startActivity(reg);
                break;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}